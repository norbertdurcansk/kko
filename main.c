/**
 * Source code main.c
 * Main application
 * Program parameters parsing, main controller
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 2/22/2017.
 * VUT FIT 2017
 * 1MIT LS KKO
 *
 */
#include <iostream>
#include <getopt.h>
#include <cstdio>
#include <cstdlib>
#include "gif2bmp.h"

using namespace std;

void printHelp() {
    fprintf(stdout,"\n\
        -i <ifile>   name of the input file <ifile>\n\
	-o <ofile>   name of the output file <ofile>\n\
	-l <logfile> name of the log file <logfile>\n\
	-h           this help message\n\
");
}
int parseCmd(int argc, char **argv,tParameters *params){
    int c;
    opterr = 0;
    while ((c = getopt (argc, argv, "i:o:l:h")) != -1)
        switch (c)
        {
            case 'i':
                params->inputFile = fopen(optarg,"rb");
                if(!params->inputFile){
                    fprintf (stderr, "Could not input file %s", optarg);
                    return -1;
                }
                break;
            case 'o':
                params->outputFile = fopen(optarg,"wb");
                if(!params->outputFile){
                    fprintf (stderr, "Could not output file %s", optarg);
                    return -1;
                }
                break;
            case 'l':
                params->logFile = fopen(optarg,"wb");
                if(!params->logFile){
                    fprintf (stderr, "Could not log file file %s", optarg);
                    return -1;
                }
                break;
            case 'h':
                printHelp();
                exit(0);
            case '?':
                if (optopt == 'i' || optopt == 'o' || optopt == 'l')
                    fprintf (stderr, " -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option '-%c'.\n", optopt);
                else
                    fprintf (stderr,
                             "Unknown option character '\\x%x'.\n",
                             optopt);
                return -1;
            default:
                return -1;
        }
    return 0;
}


int main (int argc, char **argv) {
    tParameters params;
    tGIF2BMP gifToBmp;
    if(int ret=parseCmd(argc,argv,&params)!=0)return ret;
    gif2bmp(&gifToBmp, params.inputFile, params.outputFile);
    if(params.logFile!=NULL){
       fprintf(params.logFile,"login = xdurca01\nuncodedSize = %ld\ncodedSize = %ld\n",(long)gifToBmp.gifSize,(long)gifToBmp.bmpSize);
    }
    return 0;
}
