/**
 * Source code gif2bmp.c
 * Implements library functions for GIF parsing and converting to BMP format version 4
 * library supports transparency, interlacing and resolution changes
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 2/22/2017.
 * VUT FIT 2017
 * 1MIT LS KKO
 *
 */
#include <iostream>
#include <cstdio>
#include <vector>
#include <cmath>
#include <map>
#include <cstdlib>
#include "gif2bmp.h"
#define  BMP_HEADER_SIZE 122

using namespace std;

/** Main parsing function
 *
 * @param gif2bmp  size of bmp and gif
 * @param inputFile
 * @param outputFile
 * @return if 0 ok else -1
 */
int32_t gif2bmp(tGIF2BMP *gif2bmp, FILE *inputFile, FILE *outputFile) {
    tGIF gif;
    vector<int32_t> data = loadGIF(inputFile);
    if(parseGIF(&data, &gif,gif2bmp)==-1)return -1;
    if (gif.imageDescriptor.interlaceFlag == 1) {
        gif.imageData.data = interlacing(&(gif));
    }
    if (gif.imageData.data.size() != (unsigned) (gif.imageDescriptor.height * gif.imageDescriptor.width)) {
        fprintf(stderr, "LZW decoding error\n");
        return -1;
    }
    //Not full canvas size -> add background color
    if(gif.imageDescriptor.height!=gif.height || gif.imageDescriptor.width!=gif.width) {
        gif.imageData.data=consolidate(&gif);
    }

    gif2bmp->bmpSize=gif.height*gif.width*4;
    createBMP(&gif,outputFile);
    return 0;
}

/** Consolidate data to full size of canvas
 *
 * @param gif
 * @return
 */
vector<int32_t > consolidate(tGIF *gif){
    vector<int32_t> canvasData;
    int32_t backgroundIdx = gif->backgroundColorIndex;
    for (unsigned int x = 0; x < gif->height; x++) {
        for (unsigned int y = 0; y < gif->width; y++) {
            if (x >= gif->imageDescriptor.top && x < gif->imageDescriptor.top + gif->imageDescriptor.height &&
                y >= gif->imageDescriptor.left && y < gif->imageDescriptor.left + gif->imageDescriptor.width) {
                canvasData.push_back((gif->imageData.data.at(0)));
                gif->imageData.data.erase(gif->imageData.data.begin(), gif->imageData.data.begin() + 1);
            } else {
                canvasData.push_back(backgroundIdx);
            }
        }
    }
    return canvasData;
}

/** Get row data and add to map
 *
 * @param newData
 * @param data
 * @param width
 * @param index
 */
void getRow(map<int, vector<int32_t>> *newData, vector<int32_t> *data, uint16_t width, unsigned int index) {
    vector<int32_t> row;
    unsigned int rowIdx = 0;
    for (rowIdx = 0; rowIdx < width; rowIdx++) {
        row.push_back((data->at(rowIdx)));
    }
    (*newData)[index] = row;
    data->erase(data->begin(), data->begin() + width);
}

/** Interlacing algorithm
 *
 * @param gif
 * @return vector
 */
vector<int32_t> interlacing(tGIF *gif) {
    map<int, vector<int32_t>> rowMap;
    vector<int32_t> newData;
    vector<int32_t> *data = &(gif->imageData.data);
    unsigned int index = 0;
    unsigned int i = 0;

    for (index = 0, i = 0; index < gif->height; i++, index = 8 * i) {
        getRow(&rowMap, data, gif->width, index);
    }
    for (index = 4, i = 0; index < gif->height; i++, index = 8 * i + 4) {
        getRow(&rowMap, data, gif->width, index);
    }
    for (index = 2, i = 0; index < gif->height; i++, index = 4 * i + 2) {
        getRow(&rowMap, data, gif->width, index);
    }
    for (index = 1, i = 0; index < gif->height; i++, index = 2 * i + 1) {
        getRow(&rowMap, data, gif->width, index);
    }
    for (unsigned int ix = 0; ix < gif->height; ix++) {
        vector<int32_t> vec = rowMap[ix];
        for (int32_t item:vec) {
            newData.push_back(item);
        }
    }
    return newData;
}

/** Loading gif data to vector
 *
 * @param inputFile
 * @return vector if data loaded
 */
vector<int32_t> loadGIF(FILE *inputFile) {
    vector<int32_t> data;
    int32_t ch = 0;
    while ((ch = fgetc(inputFile)) != EOF) {
        data.push_back(ch);
    }
    return data;
}

/** Parsing and decoding gif data
 *
 * @param data
 * @param gif structure
 */
int8_t parseGIF(vector<int32_t> *data, tGIF *gif,tGIF2BMP *gif2bmp) {
    //signature + version
    data->erase(data->begin(), data->begin() + 6);
    //logical screen descriptor
    getGIFLogScrDesc(data, gif);
    if (gif->globalFlag) {
        getGIFColTable(data, gif);
    }
    while (data->at(0) != 0x3b && !data->empty()) {
        //Extension processing
        if (data->at(0) == 0x21 && data->size()>2) {
            if (data->at(1) == 0xf9) {
                data->erase(data->begin(), data->begin() + 2);
                if(data->size()>6) {
                    getGIFGraphicCntExt(data, gif);
                }
            } else if (data->at(1) == 0x01 || data->at(1) == 0xFF || data->at(1) == 0xFE) {
                int32_t ext = data->at(1);
                if (ext != 0xFE) {
                    int32_t blockSize = data->at(2);
                    if (data->size() > (unsigned) (blockSize + 3)) {
                        data->erase(data->begin(), data->begin() + blockSize + 3);
                    }
                } else {
                    data->erase(data->begin(), data->begin() + 2);
                }
                int32_t subSize;
                while ((subSize = (int32_t) data->at(0)) != 0) {
                    if (data->size() > (unsigned) (subSize + 1)) {
                        data->erase(data->begin(), data->begin() + subSize + 1);
                    }else break;
                }
                data->erase(data->begin(), data->begin() + 1);
            }
        }
        //image descriptor 10 bytes
        else if (data->at(0) == 0x2C && gif->imageData.data.size()==0 && data->size()>10) {
            data->erase(data->begin(), data->begin() + 1);
            getGIFImageDesc(data, gif);
            if (gif->imageDescriptor.colorFlag && data->size() > gif->colorTable.size()) {
                getGIFColTable(data, gif);
            }
            //image data
            if(data->size()>2) {
                getGIFImageData(data, gif,gif2bmp);
            }
        } else {
            fprintf(stderr, "Wrong data format");
            return -1;
        }
    }
    return 0;
}

/** GET logical screen descriptor
 *
 * @param data
 * @param gif
 */
void getGIFLogScrDesc(vector<int32_t> *data, tGIF *gif) {
    gif->width = (uint16_t)(decToBin((*data), 2));
    (*data).erase((*data).begin(), (*data).begin() + 2);
    gif->height =(uint16_t)( decToBin((*data), 2));
    (*data).erase((*data).begin(), (*data).begin() + 2);
    gif->globalFlag = (uint8_t)(((*data).at(0) & 0b10000000) >> 7);
    gif->colorTableSize = ((uint32_t) pow(2.0, ((*data).at(0) & 0b00000111) + 1));
    gif->backgroundColorIndex =(uint8_t)( (*data).at(1));
    (*data).erase((*data).begin(), (*data).begin() + 3);
    return;
}

/** Covert multiple values to one value
 *
 * @param data
 * @param size
 * @return
 */
uint32_t decToBin(vector<int32_t> data, uint32_t size) {
    uint32_t value = 0;
    unsigned int index = 0;
    for (; index < size; index++) {
        value += data.at(index) << index * 8;
    }
    return value;
}

/** GET Global or Local color table
 *
 * @param data
 * @param gif
 */
void getGIFColTable(vector<int32_t> *data, tGIF *gif) {
    unsigned int i;
    gif->colorTable.clear();
    for (i = 0; i < (3 * gif->colorTableSize); i += 3) {
        tColor color;
        color.red = (*data).at(i);
        color.green = (*data).at(i + 1);
        color.blue = (*data).at(i + 2);
        gif->colorTable.push_back(color);
    }
    data->erase(data->begin(), data->begin() + i);
}

/** GET Graphic context extension
 *
 * @param data
 * @param gif
 */
void getGIFGraphicCntExt(vector<int32_t> *data, tGIF *gif) {
    gif->graphCntExt.size =(uint8_t) (*data).at(0);
    gif->graphCntExt.colorFlag = (uint8_t) ((*data).at(1) & 0b00000001);
    data->erase((*data).begin(), (*data).begin() + 4);
    gif->graphCntExt.colorIndex = (uint8_t)(*data).at(0);
    data->erase((*data).begin(), (*data).begin() + 2);
}

/** GET Image Description
 *
 * @param data
 * @param gif
 */
void getGIFImageDesc(vector<int32_t> *data, tGIF *gif) {

    gif->imageDescriptor.left = (uint16_t) decToBin((*data), 2);
    data->erase((*data).begin(), (*data).begin() + 2);
    gif->imageDescriptor.top = (uint16_t) decToBin((*data), 2);
    data->erase((*data).begin(), (*data).begin() + 2);
    gif->imageDescriptor.width = (uint16_t) decToBin((*data), 2);
    data->erase((*data).begin(), (*data).begin() + 2);
    gif->imageDescriptor.height = (uint16_t) decToBin((*data), 2);
    data->erase((*data).begin(), (*data).begin() + 2);
    gif->imageDescriptor.colorFlag = (uint8_t)( ((*data).at(0) & 0b10000000) >> 7);
    gif->imageDescriptor.interlaceFlag = (uint8_t)( ((*data).at(0) & 0b01000000) >> 6);
    gif->colorTableSize=((uint32_t) pow(2.0, ((*data).at(0) & 0b00000111) + 1));
    data->erase((*data).begin(), (*data).begin() + 1);
}

/** init Table , load color indexes, clear code and EOI
 *
 * @param size
 * @param table
 */
void initCodeTable(int32_t size, tImageData *imageData) {
    imageData->maxSize=-1;
    imageData->codeTable.clear();
    imageData->initFlag=true;
    for (int j = 0; j < size; j++) {
        vector<int32_t> vec;
        vec.push_back(j);
        imageData->codeTable.push_back(vec);
    }
}

/** GET Image Data
 *
 * @param data
 * @param gif
 * @return 0 if ok else error
 */
uint8_t getGIFImageData(vector<int32_t> *data, tGIF *gif,tGIF2BMP *gif2bmp) {
    gif->imageData.lzwMin =(uint8_t) (*data).at(0);
    data->erase((*data).begin(), (*data).begin() + 1);
    //init tables 2 = CC + EOI
    initCodeTable(gif->colorTable.size() + 2, &(gif->imageData));
    uint32_t size;
    gif->imageData.codeSize = (uint8_t)(gif->imageData.lzwMin + 1);
    int32_t reservedBits = 0;
    int32_t reservedValue = 0;
    gif2bmp->gifSize=0;
    while ((size = (uint32_t) (*data).at(0)) != 0  && data->size()!=0) {
        data->erase((*data).begin(), (*data).begin() + 1); // remove sizeByte
        gif2bmp->gifSize+=size;
        for (uint32_t i = 0; i < size &&  data->size()>size; i++) {
            uint8_t blockSize = 8;
            while (blockSize > 0) {
                if (gif->imageData.codeTable.size() == (uint32_t) pow(2.0, gif->imageData.codeSize) && gif->imageData.codeSize!=12) { gif->imageData.codeSize++; }
                getGIFBits(&(data->at(i)), gif, gif->imageData.codeSize, &blockSize, &reservedBits, &reservedValue);
            }
        }
        data->erase((*data).begin(), (*data).begin() + size);
    }
    data->erase((*data).begin(), (*data).begin() + 1);
    return 0;
}

/** Decompression algorithm
 *
 * @param gif
 * @param value
 */
void GIFDecompression(tGIF *gif,uint32_t value) {
    if (gif->imageData.initFlag) {
        if (value >= gif->imageData.codeTable.size()){
            fprintf(stderr, "Wrong image data");
            exit(-1);
        }
        gif->imageData.data.push_back((gif->imageData.codeTable.at(value).at(0)));
        gif->imageData.lastCode=value;
        gif->imageData.initFlag=false;
        return;
    }
    int32_t pvalue;
    //NO
    if (gif->imageData.codeTable.size() <= value) {
        vector<int32_t> tmp = (gif->imageData.codeTable.at(gif->imageData.lastCode));
        pvalue = tmp.at(0);
        tmp.push_back(pvalue);
        gif->imageData.codeTable.push_back(tmp);
        for (int32_t item: tmp) {
            gif->imageData.data.push_back(item);
        }
    //YES
    } else {
        vector<int32_t> tmp = (gif->imageData.codeTable.at(value));
        for (int32_t item: tmp) {
            gif->imageData.data.push_back(item);
        }
        pvalue = tmp.at(0);
        tmp = (gif->imageData.codeTable.at(gif->imageData.lastCode));
        tmp.push_back(pvalue);
        gif->imageData.codeTable.push_back(tmp);
    }
    gif->imageData.lastCode=value;
    return;
}

/** Push decoded data
 *
 * @param value
 * @param gif
 */
void pushGIFData(int32_t value, tGIF *gif) {

    if((uint32_t)value == gif->colorTable.size()){
        initCodeTable(gif->colorTable.size() + 2, &(gif->imageData));
        gif->imageData.codeSize=(uint8_t) (gif->imageData.lzwMin + 1);
        gif->imageData.lastCode=(uint32_t)value;
    }else if ((uint32_t)value == gif->colorTable.size() + 1 ){
        gif->imageData.maxSize=gif->imageData.codeTable.size();
    }else if (gif->imageData.maxSize!=(signed)gif->imageData.codeTable.size()){
        GIFDecompression(gif,(uint32_t)value);
    }
}

/** Decoding function LZW
 *
 * @param data
 * @param gif
 * @param size
 * @param blockBits
 * @param reservedBits
 * @param reservedValue
 * @param wordCount
 * @return
 */
void getGIFBits(int32_t *data, tGIF *gif, int8_t size, uint8_t *blockBits, int32_t *reservedBits, int32_t *reservedValue) {
    if (*reservedBits == 0) {
        if (*blockBits >= size) {
            pushGIFData((*data & (uint8_t) (pow(2.0, size) - 1)), gif);
            (*data) >>= size;
            *blockBits -= size;
        } else {
            *reservedBits = *blockBits;
            *blockBits = 0;
            *reservedValue = *data;
        }
    } else {
        if (*blockBits >= size-*reservedBits) {
            pushGIFData(((*data & (uint8_t) (pow(2.0, size - (*reservedBits)) - 1)) << (*reservedBits)) + *reservedValue, gif);
            *data = *data >> (size - *reservedBits);
            *blockBits -= (size - *reservedBits);
            *reservedBits = 0;
            *reservedValue = 0;
        } else {
            *reservedValue = (*data << *reservedBits) + *reservedValue;
            *reservedBits += 8;
            *blockBits=0;
        }
    }
    return;
}

/** Write value  on specific byte size
 *
 * @param value
 * @param bytes
 * @param output
 */
void writeToFile(int64_t value, uint8_t bytes, FILE *output){
    if(bytes == 1){
        fputc((uint8_t) value, output);
    }else{
        for(uint8_t byte=1; byte <= bytes; byte++){
            fputc((uint8_t)(value & 0xFF), output);
            value>>=8;
        }
    }
}

/** Write  decoded data to BMP file
 *
 * @param gif
 */
void createBMP(tGIF *gif,FILE *output){
    //HEADER
    writeToFile(0x42,1,output);
    writeToFile(0x4d,1,output);
    writeToFile((gif->height*gif->width*4)+BMP_HEADER_SIZE,4,output);
    writeToFile(0,4,output);
    writeToFile(BMP_HEADER_SIZE,4,output);
    //META-INF
    writeToFile(108,4,output);
    writeToFile(gif->width,4,output);
    writeToFile(gif->height,4,output);
    writeToFile(1,2,output);
    writeToFile(32,2,output); //RGB 3bytes/pixel
    writeToFile(3,4,output);
    writeToFile((gif->height*gif->width*4),4,output);
    writeToFile(0x0b13,4,output); //72 dpi
    writeToFile(0x0b13,4,output); //72 dpi
    writeToFile(0,8,output);
    // COLOR MASKS
    writeToFile(0x00FF0000,4,output);
    writeToFile(0x0000FF00,4,output);
    writeToFile(0x000000FF,4,output);
    writeToFile(0xFF000000,4,output);
    writeToFile(0x57696e20,4,output);

    writeToFile(0,48,output);
    //BITMAP DATA
    int transparentIndex=-1;
    if(gif->graphCntExt.colorFlag==1) {
        transparentIndex = gif->graphCntExt.colorIndex;
    }
    for(int x=gif->height-1;x>=0;x--){
        for(int y=0;y<gif->width;y++){
                int index =gif->imageData.data.at((unsigned)((x*gif->width)+y));
                tColor color=gif->colorTable.at((unsigned)index);
                writeToFile(color.blue,1,output);
                writeToFile(color.green,1,output);
                writeToFile(color.red,1,output);
                if(index==transparentIndex) {
                    writeToFile(0, 1, output);
                }else{
                    writeToFile(255, 1, output);
                }
            }
    }
}

