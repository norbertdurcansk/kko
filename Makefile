# Norbert Durcansky
# xdurca01
# 3.3.2017
# VUT FIT 
# KKO

CC=g++
FLAGS=-std=c++11 -g -Wall -Wextra

all: gif2bmplib.a
gif2bmplib.o: gif2bmp.h gif2bmp.c
	$(CC) $(FLAGS) -c gif2bmp.c -o gif2bmplib.o

gif2bmplib.a: gif2bmplib.o
	ar -cvq gif2bmplib.a gif2bmplib.o

all: gif2bmp
gif2bmp.o: main.c gif2bmp.h
	$(CC) $(FLAGS) -c main.c -o gif2bmp.o

gif2bmp: gif2bmp.o gif2bmplib.a
	$(CC) $(FLAGS) -o gif2bmp gif2bmp.o gif2bmplib.a

clean:
	rm -f *.o gif2bmp *.a
	rm -f *~
