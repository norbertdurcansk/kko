/**
 * Source code gif2bmp.h
 * Defines library functions and structures for GIF parsing and converting to BMP format version 4
 * library supports transparency, interlacing and resolution changes
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 2/22/2017.
 * VUT FIT 2017
 * 1MIT LS KKO
 *
 */
#ifndef KKO_GIF2BMP_H
#define KKO_GIF2BMP_H

#include <vector>
#include <set>
#include <map>

/**
 * Implemented structure
 */
typedef struct{
    int64_t bmpSize;
    int64_t long gifSize;
} tGIF2BMP;

/**
 * Program parameters structure
 */
typedef struct{
    FILE *inputFile=stdin;
    FILE *outputFile=stdout;
    FILE *logFile=NULL;
} tParameters;

/**
 * Color structure
 */
typedef struct{
    int32_t red;
    int32_t green;
    int32_t blue;
}tColor;

/**
 * Graphic control extension structure
 */
typedef struct{
uint8_t size;
uint8_t colorFlag=0;
uint8_t colorIndex;
}tGraphCntExt;

/**
 * Image descriptor structure
 */
typedef struct{
    uint16_t left;
    uint16_t top;
    uint16_t width;
    uint16_t height;
    uint8_t colorFlag=0;
    uint8_t interlaceFlag=0;
}tImageDescriptor;

/**
 * Image data structure
 */
typedef struct{
    uint8_t lzwMin;
    uint8_t codeSize;
    uint32_t lastCode;
    std::vector<int32_t> data;
    int maxSize=-1;
    bool initFlag=true;
    std::vector<std::vector<int32_t>>codeTable;
}tImageData;

/**
 * Main GIF structure
 */
typedef struct{
    uint16_t width;
    uint16_t height;
    uint8_t globalFlag=0;
    uint32_t colorTableSize=0;
    uint8_t  backgroundColorIndex=0;
    std::vector<tColor> colorTable;
    tGraphCntExt graphCntExt;
    tImageDescriptor imageDescriptor;
    tImageData imageData;
} tGIF;


/**
 * Library functions d
 */
int32_t gif2bmp(tGIF2BMP *gif2bmp, FILE *inputFile, FILE *outputFile);
void getGIFBits(int32_t *data,tGIF *gif,int8_t size,uint8_t *blockBits,int32_t *reservedBits,int32_t *reservedValue);
std::vector<int32_t> loadGIF(FILE *inputFile);
void GIFDecompression(tGIF *gif,uint32_t value);
int8_t parseGIF(std::vector<int32_t> *data, tGIF *gif,tGIF2BMP *gif2bmp);
void getGIFLogScrDesc(std::vector<int32_t> *data, tGIF *gif);
void getGIFColTable(std::vector<int32_t> *data, tGIF *gif);
void getGIFGraphicCntExt(std::vector<int32_t> *data, tGIF *gif);
void getGIFImageDesc(std::vector<int32_t> *data, tGIF *gif);
uint8_t getGIFImageData(std::vector<int32_t> *data, tGIF *gif,tGIF2BMP *gif2bmp);
void initCodeTable(int32_t size, tImageData *imageData);
std::vector<int32_t > consolidate(tGIF *gif);
void getRow(std::map<int,std::vector<int32_t>> *newData,std::vector<int32_t> *data,uint16_t width,unsigned int index);
std::vector<int32_t> interlacing(tGIF *gif);
uint32_t decToBin(std::vector<int32_t> data, uint32_t size);
void createBMP(tGIF *gif,FILE *output);

#endif //KKO_GIF2BMP_H
